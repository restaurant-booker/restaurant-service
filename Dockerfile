FROM openjdk:11-jdk
EXPOSE 8080
ADD target/${project.artifactId}-${project.version}.jar ${project.artifactId}-${project.version}.jar
ENTRYPOINT ["java", "-jar", "${project.artifactId}-${project.version}.jar"]

