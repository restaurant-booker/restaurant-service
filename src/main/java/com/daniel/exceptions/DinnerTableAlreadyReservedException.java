package com.daniel.exceptions;

public class DinnerTableAlreadyReservedException extends RuntimeException {

    public DinnerTableAlreadyReservedException() {
        super("This dinner table is already reserved at this time.");
    }
}
