package com.daniel.exceptions;

public class RestaurantNotOpenException extends RuntimeException {

    public RestaurantNotOpenException() {
        super("This restaurant is not open on this day.");
    }
}
