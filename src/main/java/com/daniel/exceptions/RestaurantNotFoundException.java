package com.daniel.exceptions;

public class RestaurantNotFoundException extends RuntimeException {

    public RestaurantNotFoundException() {
        super("The required restaurant has not been found.");
    }
}
