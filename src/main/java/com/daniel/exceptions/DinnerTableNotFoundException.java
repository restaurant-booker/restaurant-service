package com.daniel.exceptions;

public class DinnerTableNotFoundException extends RuntimeException{

    public DinnerTableNotFoundException() {
        super("The required dinner table has not been found.");
    }
}
