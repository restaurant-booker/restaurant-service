package com.daniel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table
public class DinnerTable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "table_id_generator")
    @SequenceGenerator(name="table_id_generator", allocationSize=1)
    @EqualsAndHashCode.Exclude
    private Long id;
    private Integer seats;
    @OneToMany(mappedBy = "dinnerTable", cascade = CascadeType.ALL)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Reservation> reservations;
    @ManyToOne
    @JoinColumn(name = "restaurantId")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Restaurant restaurant;
}
