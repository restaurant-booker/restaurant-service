package com.daniel.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_id_generator")
    @SequenceGenerator(name="address_id_generator", allocationSize=1)
    @EqualsAndHashCode.Exclude
    private Long id;
    @OneToOne(mappedBy = "address")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Restaurant restaurant;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String addressLine4;
    private String postcode;
    private String country;
    private String city;
    private String telephoneNumber;
}
