package com.daniel.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Category {

    JAPANESE, AMERICAN, ITALIAN
}
