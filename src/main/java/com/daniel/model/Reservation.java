package com.daniel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.sql.Date;
import java.sql.Time;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reservation_id_generator")
    @SequenceGenerator(name="reservation_id_generator", allocationSize=1)
    @EqualsAndHashCode.Exclude
    private Long id;
    private Date date;
    private Time startTime;
    @ManyToOne
    @JoinColumn(name = "dinnerTableId")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private DinnerTable dinnerTable;
    @ManyToOne
    @JoinColumn(name = "restaurantId")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Restaurant restaurant;
}
