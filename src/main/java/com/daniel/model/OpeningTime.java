package com.daniel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.sql.Time;
import java.time.DayOfWeek;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table
public class OpeningTime {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "opening_time_id_generator")
    @SequenceGenerator(name="opening_time_id_generator", allocationSize=1)
    @EqualsAndHashCode.Exclude
    private Long id;
    private DayOfWeek dayOfWeek;
    private Time startTime;
    private Time closingTime;
    @ManyToOne
    @JoinColumn(name = "restaurantId")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Restaurant restaurant;
}
