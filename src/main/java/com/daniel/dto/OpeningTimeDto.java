package com.daniel.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.time.DayOfWeek;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OpeningTimeDto {

    private Long id;
    private DayOfWeek dayOfWeek;
    private Time startTime;
    private Time closingTime;
}
