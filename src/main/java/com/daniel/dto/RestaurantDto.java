package com.daniel.dto;

import com.daniel.model.Category;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestaurantDto {

    @ApiModelProperty(example = "1")
    private Long id;
    @ApiModelProperty(example = "Papa Johns")
    private String name;
    private AddressDto address;
    @ApiModelProperty(example = "AMERICAN")
    private Set<Category> categories;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ReservationDto> reservations;
    private List<DinnerTableDto> dinnerTables;
    private Set<OpeningTimeDto> openingTimes;
}
