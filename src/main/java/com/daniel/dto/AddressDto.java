package com.daniel.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddressDto {

    @ApiModelProperty(example = "1")
    private Long id;
    @ApiModelProperty(example = "Street 12")
    @JsonProperty(required = true)
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String addressLine4;
    @ApiModelProperty(example = "61-666")
    @JsonProperty(required = true)
    private String postcode;
    @JsonProperty(required = true)
    @ApiModelProperty(example = "Poland")
    private String country;
    @JsonProperty(required = true)
    @ApiModelProperty(example = "Poznan")
    private String city;
    @JsonProperty(required = true)
    @ApiModelProperty(example = "+48 123 456 789")
    private String telephoneNumber;
}
