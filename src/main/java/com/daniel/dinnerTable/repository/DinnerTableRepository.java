package com.daniel.dinnerTable.repository;

import com.daniel.model.DinnerTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DinnerTableRepository extends JpaRepository<DinnerTable, Long> {
}
