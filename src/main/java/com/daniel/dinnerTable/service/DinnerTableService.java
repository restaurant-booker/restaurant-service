package com.daniel.dinnerTable.service;

import com.daniel.dinnerTable.repository.DinnerTableRepository;
import com.daniel.exceptions.DinnerTableAlreadyReservedException;
import com.daniel.exceptions.DinnerTableNotFoundException;
import com.daniel.model.DinnerTable;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;

@Service
@AllArgsConstructor
public class DinnerTableService {

    private DinnerTableRepository dinnerTableRepository;

    public DinnerTable returnTableIfNotReserved(Long dinnerTableId, Date date, Time startTime) {
        DinnerTable dinnerTable = dinnerTableRepository.findById(dinnerTableId)
                                                       .orElseThrow(DinnerTableNotFoundException::new);
        if (isReservationAvailableOnDateAndTime(date, startTime, dinnerTable)) {
            return dinnerTable;
        } else {
            throw new DinnerTableAlreadyReservedException();
        }
    }

    private static boolean isReservationAvailableOnDateAndTime(Date date, Time startTime, DinnerTable dinnerTable) {
        return dinnerTable.getReservations()
                          .parallelStream()
                          .noneMatch(reservation -> reservation.getDate()
                                                               .equals(date)
                                  && reservation.getStartTime()
                                                .before(Time.valueOf(startTime.toLocalTime()
                                                                              .plusHours(2)))
                                  && reservation.getStartTime()
                                                .after(Time.valueOf(startTime.toLocalTime()
                                                                             .minusHours(2))));
    }
}
