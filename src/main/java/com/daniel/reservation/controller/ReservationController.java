package com.daniel.reservation.controller;

import com.daniel.dto.ReservationDto;
import com.daniel.reservation.service.ReservationService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
@AllArgsConstructor
public class ReservationController implements ReservationContract {

    private ReservationService reservationService;

    @PostMapping("/reservations")
    public ResponseEntity<ReservationDto> addReservation(@RequestBody ReservationDto reservationDto) {
        ReservationDto savedReservation = reservationService.addReservation(reservationDto);
        return ResponseEntity.created(URI.create("/reservations/" + savedReservation.getId()))
                             .body(savedReservation);
    }
}
