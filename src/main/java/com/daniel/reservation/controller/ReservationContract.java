package com.daniel.reservation.controller;

import com.daniel.dto.ReservationDto;
import com.daniel.exceptions.DinnerTableAlreadyReservedException;
import com.daniel.exceptions.DinnerTableNotFoundException;
import com.daniel.exceptions.RestaurantNotFoundException;
import com.daniel.exceptions.RestaurantNotOpenException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface ReservationContract {

    @ApiOperation(value = "Add a reservation.")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "A reservation has been added.", response = ReservationDto.class),
            @ApiResponse(code = 403, message = "This restaurant is not open on this day.", response = RestaurantNotOpenException.class),
            @ApiResponse(code = 403, message = "This dinner table is already reserved at this time.", response = DinnerTableAlreadyReservedException.class),
            @ApiResponse(code = 404, message = "The required restaurant has not been found.", response = RestaurantNotFoundException.class),
            @ApiResponse(code = 404, message = "The required dinner table has not been found.", response = DinnerTableNotFoundException.class)})
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/reservation")
    ResponseEntity<ReservationDto> addReservation(@RequestBody ReservationDto reservationDto);
}
