package com.daniel.reservation.service;

import com.daniel.dinnerTable.service.DinnerTableService;
import com.daniel.dto.ReservationDto;
import com.daniel.exceptions.RestaurantNotFoundException;
import com.daniel.mappers.ReservationMapper;
import com.daniel.model.DinnerTable;
import com.daniel.model.Reservation;
import com.daniel.model.Restaurant;
import com.daniel.openingTime.service.OpeningTimeService;
import com.daniel.reservation.repository.ReservationRepository;
import com.daniel.restaurant.repository.RestaurantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ReservationService {

    private RestaurantRepository restaurantRepository;
    private ReservationRepository reservationRepository;
    private OpeningTimeService openingTimeService;
    private ReservationMapper reservationMapper;
    private DinnerTableService dinnerTableService;

    public ReservationDto addReservation(ReservationDto reservationDto) {

        openingTimeService.checkIfRestaurantIsOpenOnDate(reservationDto.getDate());

        DinnerTable dinnerTable = dinnerTableService.returnTableIfNotReserved(reservationDto.getDinnerTable()
                                                                                            .getId(),
                                                                              reservationDto.getDate(),
                                                                              reservationDto.getStartTime());

        Reservation reservation = reservationMapper.convertDtoToEntity(reservationDto);
        Restaurant restaurant = restaurantRepository.findById(reservationDto.getRestaurant()
                                                                            .getId())
                                                    .orElseThrow(RestaurantNotFoundException::new);

        reservation.setDinnerTable(dinnerTable);
        reservation.setRestaurant(restaurant);

        return reservationMapper.convertEntityToDto(reservationRepository.save(reservation));
    }
}
