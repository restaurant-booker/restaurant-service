package com.daniel.mappers;

import com.daniel.dto.AddressDto;
import com.daniel.model.Address;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    AddressDto convertEntityToDto(Address address) {

        return AddressDto.builder()
                         .id(address.getId())
                         .addressLine1(address.getAddressLine1())
                         .addressLine2(address.getAddressLine2())
                         .addressLine3(address.getAddressLine3())
                         .addressLine4(address.getAddressLine4())
                         .city(address.getCity())
                         .country(address.getCountry())
                         .postcode(address.getPostcode())
                         .telephoneNumber(address.getTelephoneNumber())
                         .build();
    }

    Address convertDtoToEntity(AddressDto addressDto) {

        return Address.builder()
                      .id(addressDto.getId())
                      .addressLine1(addressDto.getAddressLine1())
                      .addressLine2(addressDto.getAddressLine2())
                      .addressLine3(addressDto.getAddressLine3())
                      .addressLine4(addressDto.getAddressLine4())
                      .city(addressDto.getCity())
                      .country(addressDto.getCountry())
                      .postcode(addressDto.getPostcode())
                      .telephoneNumber(addressDto.getTelephoneNumber())
                      .build();
    }
}
