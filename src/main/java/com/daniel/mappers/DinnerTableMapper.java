package com.daniel.mappers;

import com.daniel.dto.DinnerTableDto;
import com.daniel.model.DinnerTable;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class DinnerTableMapper {

    DinnerTableDto convertEntityToDto(DinnerTable dinnerTable) {

        return DinnerTableDto.builder()
                             .id(dinnerTable.getId())
                             .seats(dinnerTable.getSeats())
                             .build();
    }

    DinnerTable convertDtoToEntity(DinnerTableDto dinnerTableDto) {

        return DinnerTable.builder()
                          .id(dinnerTableDto.getId())
                          .seats(dinnerTableDto.getSeats())
                          .build();
    }
}
