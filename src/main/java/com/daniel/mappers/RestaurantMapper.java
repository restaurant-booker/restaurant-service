package com.daniel.mappers;


import com.daniel.dto.RestaurantDto;
import com.daniel.model.Address;
import com.daniel.model.DinnerTable;
import com.daniel.model.OpeningTime;
import com.daniel.model.Restaurant;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class RestaurantMapper {

    private AddressMapper addressMapper;
    private DinnerTableMapper dinnerTableMapper;
    private OpeningTimeMapper openingTimeMapper;
    private ReservationMapper reservationMapper;

    public RestaurantDto convertEntityToDto(Restaurant restaurant) {

        return RestaurantDto.builder()
                            .id(restaurant.getId())
                            .name(restaurant.getName())
                            .address(Optional.ofNullable(restaurant.getAddress())
                                             .map(address -> addressMapper.convertEntityToDto(address))
                                             .orElse(null))
                            .categories(restaurant.getCategories())
                            .dinnerTables(Optional.ofNullable(restaurant.getDinnerTables())
                                                  .map(dinnerTables -> dinnerTables.parallelStream()
                                                                                   .map(dinnerTable -> dinnerTableMapper.convertEntityToDto(dinnerTable))
                                                                                   .collect(Collectors.toList()))
                                                  .orElse(null))
                            .openingTimes(Optional.ofNullable(restaurant.getOpeningTimes())
                                                  .map(openingTimes -> openingTimes.parallelStream()
                                                                                   .map(openingTime -> openingTimeMapper.convertEntityToDto(openingTime))
                                                                                   .collect(Collectors.toSet()))
                                                  .orElse(null))
                            .reservations(Optional.ofNullable(restaurant.getReservations())
                                                  .map(reservations -> reservations.parallelStream()
                                                                                   .map(reservation -> reservationMapper.convertEntityToDto(reservation))
                                                                                   .collect(Collectors.toList()))
                                                  .orElse(null))
                            .build();
    }

    public Restaurant convertDtoToEntity(RestaurantDto restaurantDto) {

        Restaurant restaurant = Restaurant.builder()
                                          .id(restaurantDto.getId())
                                          .name(restaurantDto.getName())
                                          .categories(restaurantDto.getCategories())
                                          .build();

        Address address = Optional.ofNullable(restaurantDto.getAddress())
                                  .map(addressDto -> {
                                      Address entity = addressMapper.convertDtoToEntity(addressDto);
                                      entity.setRestaurant(restaurant);
                                      return entity;
                                  })
                                  .orElse(null);


        List<DinnerTable> dinnerTables = Optional.ofNullable(restaurantDto.getDinnerTables())
                                                 .map(dinnerTableList -> dinnerTableList.parallelStream()
                                                                                        .map(dinnerTable -> {
                                                                                            DinnerTable entity = dinnerTableMapper.convertDtoToEntity(dinnerTable);
                                                                                            entity.setRestaurant(restaurant);
                                                                                            return entity;
                                                                                        })
                                                                                        .collect(Collectors.toList()))
                                                 .orElse(Collections.emptyList());

        Set<OpeningTime> openingTimes = Optional.ofNullable(restaurantDto.getOpeningTimes())
                                                .map(openingTimeSet -> openingTimeSet.parallelStream()
                                                                                     .map(openingTime -> {
                                                                                         OpeningTime entity = openingTimeMapper.convertDtoToEntity(openingTime);
                                                                                         entity.setRestaurant(restaurant);
                                                                                         return entity;
                                                                                     })
                                                                                     .collect(Collectors.toSet()))
                                                .orElse(Collections.emptySet());
        restaurant.setAddress(address);
        restaurant.setDinnerTables(dinnerTables);
        restaurant.setOpeningTimes(openingTimes);
        return restaurant;
    }
}
