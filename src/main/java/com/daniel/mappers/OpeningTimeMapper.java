package com.daniel.mappers;

import com.daniel.dto.OpeningTimeDto;
import com.daniel.model.OpeningTime;
import org.springframework.stereotype.Component;

@Component
public class OpeningTimeMapper {

    OpeningTimeDto convertEntityToDto(OpeningTime openingTime) {

        return OpeningTimeDto.builder()
                             .id(openingTime.getId())
                             .dayOfWeek(openingTime.getDayOfWeek())
                             .closingTime(openingTime.getClosingTime())
                             .startTime(openingTime.getStartTime())
                             .build();
    }

    OpeningTime convertDtoToEntity(OpeningTimeDto openingTimeDto) {

        return OpeningTime.builder()
                          .id(openingTimeDto.getId())
                          .dayOfWeek(openingTimeDto.getDayOfWeek())
                          .closingTime(openingTimeDto.getClosingTime())
                          .startTime(openingTimeDto.getStartTime())
                          .build();
    }
}
