package com.daniel.mappers;

import com.daniel.dto.DinnerTableDto;
import com.daniel.dto.ReservationDto;
import com.daniel.dto.RestaurantDto;
import com.daniel.model.Reservation;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ReservationMapper {

    public ReservationDto convertEntityToDto(Reservation reservation) {

        return ReservationDto.builder()
                             .id(reservation.getId())
                             .date(reservation.getDate())
                             .startTime(reservation.getStartTime())
                             .restaurant(Optional.ofNullable(reservation.getRestaurant())
                                                 .map(restaurant -> RestaurantDto.builder()
                                                                                    .id(restaurant.getId())
                                                                                    .name(restaurant.getName())
                                                                                    .build())
                                                 .orElse(null))
                             .dinnerTable(Optional.ofNullable(reservation.getDinnerTable())
                                                  .map(dinnerTable -> DinnerTableDto.builder()
                                                                                       .id(dinnerTable.getId())
                                                                                       .seats(dinnerTable.getSeats())
                                                                                       .build())
                                                  .orElse(null))
                             .build();
    }

    public Reservation convertDtoToEntity(ReservationDto reservationDto) {

        return Reservation.builder()
                          .id(reservationDto.getId())
                          .date(reservationDto.getDate())
                          .startTime(reservationDto.getStartTime())
                          .build();
    }
}
