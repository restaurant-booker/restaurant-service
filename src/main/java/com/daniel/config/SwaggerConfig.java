package com.daniel.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.stream.Collectors;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${info.project.version}")
    private String version;
    @Value("${info.project.description}")
    private String description;
    @Value("${info.project.title}")
    private String title;
    @Value("${info.project.groupName}")
    private String groupName;

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName(groupName)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(groupName))
                .paths(PathSelectors.any())
                .build();

    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(adjustTitle(title))
                .description(description)
                .version(version)
                .contact(new Contact("Daniel Grzelak",
                                     "https://www.linkedin.com/in/daniel-grzelak/",
                                     "daniel.grzelak78@gmail.com"))
                .build();
    }

    private String adjustTitle(String title) {
        return Arrays.stream(title.split("-"))
                     .map(word -> word.replaceFirst(Character.toString(word.charAt(0)),
                                                     Character.toString(Character.toUpperCase(word.charAt(0)))))
                     .collect(Collectors.joining(" "));
    }
}
