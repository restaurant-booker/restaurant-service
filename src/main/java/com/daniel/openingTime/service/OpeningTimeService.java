package com.daniel.openingTime.service;

import com.daniel.exceptions.RestaurantNotOpenException;
import com.daniel.model.OpeningTime;
import com.daniel.openingTime.repository.OpeningTimeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
@AllArgsConstructor
public class OpeningTimeService {

    private OpeningTimeRepository openingTimeRepository;

    public OpeningTime checkIfRestaurantIsOpenOnDate(Date date) {

       return openingTimeRepository.findFirstByDayOfWeek(date.toLocalDate()
                                                       .getDayOfWeek())
                             .orElseThrow(RestaurantNotOpenException::new);
    }
}
