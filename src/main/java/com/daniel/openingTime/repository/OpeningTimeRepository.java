package com.daniel.openingTime.repository;

import com.daniel.model.OpeningTime;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.DayOfWeek;
import java.util.Optional;

public interface OpeningTimeRepository extends JpaRepository<OpeningTime, Long> {
    Optional<OpeningTime> findFirstByDayOfWeek(DayOfWeek dayOfWeek);
}
