package com.daniel.restaurant.repository;

import com.daniel.model.Category;
import com.daniel.model.OpeningTime;
import com.daniel.model.OpeningTime_;
import com.daniel.model.Reservation;
import com.daniel.model.Reservation_;
import com.daniel.model.Restaurant;
import com.daniel.model.Restaurant_;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.SetJoin;
import java.sql.Date;
import java.sql.Time;


@Component
public class RestaurantSpecs {

    public Specification<Restaurant> availableRestaurants(Date date, Time startTime) {
        if (date == null || startTime == null) {
            return null;
        }
        return Specification.where(reservationsAreEmpty().and(reservationFitsInOpeningTime(date, startTime))
                                                         .or(theReservationTimeIsNotColliding(date, startTime)));
    }

    private static Specification<Restaurant> reservationsAreEmpty() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isEmpty(root.get(Restaurant_.reservations));
    }

    private static Specification<Restaurant> theReservationTimeIsNotColliding(Date date, Time startTime) {
        return Specification.where(reservationFitsInOpeningTime(date, startTime)
                                           .and(reservationTimeDoesntCollideWithTheAdjacentReservation(startTime))
                                           .and(dateIsTheSame(date)))
                            .or(reservationFitsInOpeningTime(date, startTime).and(thereIsNoReservationOnDate(date)));
    }

    private static Specification<Restaurant> reservationTimeDoesntCollideWithTheAdjacentReservation(Time startTime) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            criteriaQuery.distinct(true);
            return criteriaBuilder.between(root.join(Restaurant_.reservations, JoinType.LEFT)
                                        .get(Reservation_.startTime), startTime,
                                    calculateReservationEndTime(startTime))
                           .not();
        };
    }

    private static Specification<Restaurant> thereIsNoReservationOnDate(Date date) {
        return (root, criteriaQuery, criteriaBuilder) -> {
           ListJoin<Restaurant, Reservation> reservationJoin = root.join(Restaurant_.reservations, JoinType.LEFT);
           return criteriaBuilder.notEqual(reservationJoin.get(Reservation_.date), date);
        };
    }

    private static Specification<Restaurant> reservationFitsInOpeningTime(Date date, Time startTime) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            SetJoin<Restaurant, OpeningTime> openingTimeJoin = root.join(Restaurant_.openingTimes);
            return criteriaBuilder.and(criteriaBuilder.equal(openingTimeJoin.get(OpeningTime_.dayOfWeek), date.toLocalDate().getDayOfWeek()),
                    criteriaBuilder.and(criteriaBuilder.lessThanOrEqualTo(openingTimeJoin.get(OpeningTime_.startTime),
                                                                                             startTime),
                                                           criteriaBuilder.greaterThanOrEqualTo(openingTimeJoin.get(OpeningTime_.closingTime),
                                                                                                calculateReservationEndTime(startTime)))
            );
        };
    }

    private static Specification<Restaurant> dateIsTheSame(Date date) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.join(Restaurant_.reservations, JoinType.LEFT).get(Reservation_.date), date);
    }

    public Specification<Restaurant> nameIsLike(String name) {
        if (name == null) {
            return null;
        }
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get(Restaurant_.name), "%" + name + "%");
    }

    public Specification<Restaurant> categoryEquals(Category category) {
        if (category == null) {
            return null;
        }
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isMember(category, root.get(Restaurant_.categories));
    }

    private static Time calculateReservationEndTime(Time startTime) {
        return Time.valueOf(startTime.toLocalTime()
                                     .plusHours(2));
    }
}
