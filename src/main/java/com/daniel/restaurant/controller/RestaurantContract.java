package com.daniel.restaurant.controller;

import com.daniel.dto.RestaurantDto;
import com.daniel.model.Category;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.sql.Date;
import java.sql.Time;
import java.util.List;


public interface RestaurantContract {

    @ApiOperation(value = "Get restaurant by ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "A restaurant with a given id.", response = RestaurantDto.class),
            @ApiResponse(code = 404, message = "Restaurant has not been found.")})
    @GetMapping("/restaurants/{id}")
    ResponseEntity<RestaurantDto> getRestaurantById(@PathVariable Long id);

    @ApiOperation(value = "Add a restaurant.")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "A restaurant has been added.", response = RestaurantDto.class)})
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/restaurants")
    ResponseEntity<RestaurantDto> addRestaurant(@RequestBody RestaurantDto restaurantDto);

    @ApiOperation(value = "Get all restaurants with filter.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "A list of all available restaurants matching the filter.", response = RestaurantDto.class)})
    @GetMapping("/restaurants")
    List<RestaurantDto> getAllRestaurantsBy(@RequestParam(required = false) String name,
                                            @RequestParam(required = false) Category category,
                                            @RequestParam(required = false) Date date,
                                            @RequestParam(required = false) Time startTime);
}
