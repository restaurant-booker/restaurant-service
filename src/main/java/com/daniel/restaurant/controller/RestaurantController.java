package com.daniel.restaurant.controller;

import com.daniel.dto.RestaurantDto;
import com.daniel.model.Category;
import com.daniel.restaurant.service.RestaurantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.sql.Date;
import java.sql.Time;
import java.util.List;


@RestController
@AllArgsConstructor
public class RestaurantController implements RestaurantContract {

    private RestaurantService restaurantService;

    @GetMapping("/restaurants")
    public List<RestaurantDto> getAllRestaurantsBy(@RequestParam(required = false) String name,
                                                   @RequestParam(required = false) Category category,
                                                   @RequestParam(required = false) Date date,
                                                   @RequestParam(required = false) Time startTime) {
        return restaurantService.getAllRestaurantsBy(name,
                                                     category,
                                                     date,
                                                     startTime);
    }

    @GetMapping("/restaurants/{id}")
    public ResponseEntity<RestaurantDto> getRestaurantById(@PathVariable Long id) {
        return ResponseEntity.of(restaurantService.getRestaurant(id));
    }

    @PostMapping("/restaurants")
    public ResponseEntity<RestaurantDto> addRestaurant(@RequestBody RestaurantDto restaurantDto) {
        RestaurantDto savedRestaurant = restaurantService.addRestaurant(restaurantDto);
        return ResponseEntity.created(URI.create("/restaurants/" + savedRestaurant.getId()))
                             .body(savedRestaurant);
    }
}
