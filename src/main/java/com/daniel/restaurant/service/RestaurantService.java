package com.daniel.restaurant.service;

import com.daniel.dto.RestaurantDto;
import com.daniel.mappers.RestaurantMapper;
import com.daniel.model.Category;
import com.daniel.model.Restaurant;
import com.daniel.restaurant.repository.RestaurantRepository;
import com.daniel.restaurant.repository.RestaurantSpecs;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.domain.Specification.where;

@Service
@AllArgsConstructor
public class RestaurantService {

    private RestaurantRepository restaurantRepository;
    private RestaurantMapper restaurantMapper;
    private RestaurantSpecs restaurantSpecs;

    public RestaurantDto addRestaurant(RestaurantDto restaurantDto) {

        Restaurant savedRestaurant = restaurantRepository.save(restaurantMapper.convertDtoToEntity(restaurantDto));

        return restaurantMapper.convertEntityToDto(savedRestaurant);
    }

    public Optional<RestaurantDto> getRestaurant(Long id) {

        return restaurantRepository.findById(id)
                                   .map(restaurantMapper::convertEntityToDto);
    }

    public List<RestaurantDto> getAllRestaurantsBy(String name, Category category, Date date, Time startTime) {

       return restaurantRepository.findAll(where(restaurantSpecs.availableRestaurants(date, startTime)).and(restaurantSpecs.nameIsLike(name))
                                                                                                 .and(restaurantSpecs.categoryEquals(category)))
                            .parallelStream()
                            .map(restaurantMapper::convertEntityToDto)
                            .collect(Collectors.toList());
    }
}
