package com.daniel.openingTime.service;

import com.daniel.exceptions.RestaurantNotOpenException;
import com.daniel.model.OpeningTime;
import com.daniel.openingTime.repository.OpeningTimeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OpeningTimeServiceTest {

    @Mock
    private OpeningTimeRepository openingTimeRepository;
    @InjectMocks
    private OpeningTimeService openingTimeService;

    @Test
    void checkIfRestaurantIsOpenOnDate() {
        OpeningTime openingTime = OpeningTime.builder()
                                             .build();
        when(openingTimeRepository.findFirstByDayOfWeek(any())).thenReturn(Optional.of(openingTime));
        OpeningTime isRestaurantOpen = openingTimeService.checkIfRestaurantIsOpenOnDate(Date.valueOf(LocalDate.now()));

        assertThat(isRestaurantOpen).isEqualTo(openingTime);
        verify(openingTimeRepository, times(1)).findFirstByDayOfWeek(any());
    }

    @Test
    void checkIfRestaurantIsOpenOnDate_throwsRestaurantNotOpenException() {
        when(openingTimeRepository.findFirstByDayOfWeek(any())).thenReturn(Optional.empty());

        assertThrows(RestaurantNotOpenException.class, () -> openingTimeService.checkIfRestaurantIsOpenOnDate(Date.valueOf(LocalDate.now())));
        verify(openingTimeRepository, times(1)).findFirstByDayOfWeek(any());
    }
}