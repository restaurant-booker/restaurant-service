package com.daniel.dinnerTable.service;

import com.daniel.dinnerTable.repository.DinnerTableRepository;
import com.daniel.exceptions.DinnerTableAlreadyReservedException;
import com.daniel.exceptions.DinnerTableNotFoundException;
import com.daniel.model.DinnerTable;
import com.daniel.model.Reservation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DinnerTableServiceTest {

    @Mock
    private DinnerTableRepository dinnerTableRepository;
    @InjectMocks
    private DinnerTableService dinnerTableService;

    @Test
    void returnTableIfNotReserved_previousReservationIsLater() {
        when(dinnerTableRepository.findById(any())).thenReturn(Optional.of(DinnerTable.builder()
                                                                                      .reservations(Collections.singletonList(Reservation.builder()
                                                                                                                                         .date(Date.valueOf(LocalDate.now()))
                                                                                                                                         .startTime(Time.valueOf(LocalTime.of(20, 0, 0)))
                                                                                                                                         .build()))
                                                                                      .build()));
        DinnerTable dinnerTable = dinnerTableService.returnTableIfNotReserved(1L, Date.valueOf(LocalDate.now()), Time.valueOf(LocalTime.of(16, 0, 0)));

        assertThat(dinnerTable).isNotNull();
        verify(dinnerTableRepository, times(1)).findById(any());
    }

    @Test
    void returnTableIfNotReserved_previousReservationIsEarlier() {
        when(dinnerTableRepository.findById(any())).thenReturn(Optional.of(DinnerTable.builder()
                                                                                      .reservations(Collections.singletonList(Reservation.builder()
                                                                                                                                         .date(Date.valueOf(LocalDate.now()))
                                                                                                                                         .startTime(Time.valueOf(LocalTime.of(16, 0, 0)))
                                                                                                                                         .build()))
                                                                                      .build()));
        DinnerTable dinnerTable = dinnerTableService.returnTableIfNotReserved(1L, Date.valueOf(LocalDate.now()), Time.valueOf(LocalTime.of(20, 0, 0)));

        assertThat(dinnerTable).isNotNull();
        verify(dinnerTableRepository, times(1)).findById(any());
    }

    @Test
    void returnTableIfNotReserved_returnsTableIfReservedOnDIfferentDay() {
        when(dinnerTableRepository.findById(any())).thenReturn(Optional.of(DinnerTable.builder()
                                                                                      .reservations(Collections.singletonList(Reservation.builder()
                                                                                                                                         .date(Date.valueOf(LocalDate.now()
                                                                                                                                                                     .minusDays(1)))
                                                                                                                                         .startTime(Time.valueOf(LocalTime.of(20, 0, 0)))
                                                                                                                                         .build()))
                                                                                      .build()));
        DinnerTable dinnerTable = dinnerTableService.returnTableIfNotReserved(1L, Date.valueOf(LocalDate.now()), Time.valueOf(LocalTime.of(16, 0, 0)));

        assertThat(dinnerTable).isNotNull();
        verify(dinnerTableRepository, times(1)).findById(any());
    }

    @Test
    void returnTableIfNotReserved_throwsExceptionIfReservationIsAlreadyMade() {
        when(dinnerTableRepository.findById(any())).thenReturn(Optional.of(DinnerTable.builder()
                                                                                      .reservations(Collections.singletonList(Reservation.builder()
                                                                                                                                         .date(Date.valueOf(LocalDate.now()))
                                                                                                                                         .startTime(Time.valueOf(LocalTime.of(20, 0, 0)))
                                                                                                                                         .build()))
                                                                                      .build()));

        assertThrows(DinnerTableAlreadyReservedException.class, () -> dinnerTableService.returnTableIfNotReserved(1L, Date.valueOf(LocalDate.now()), Time.valueOf(LocalTime.of(20, 0, 0))));
        verify(dinnerTableRepository, times(1)).findById(any());
    }

    @Test
    void returnTableIfNotReserved_throwsExceptionIfTableDoesntExist() {
        when(dinnerTableRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(DinnerTableNotFoundException.class, () -> dinnerTableService.returnTableIfNotReserved(1L, Date.valueOf(LocalDate.now()), Time.valueOf(LocalTime.of(20, 0, 0))));
        verify(dinnerTableRepository, times(1)).findById(any());
    }
}