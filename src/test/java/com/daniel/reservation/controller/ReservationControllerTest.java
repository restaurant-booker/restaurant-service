package com.daniel.reservation.controller;

import com.daniel.dto.ReservationDto;
import com.daniel.reservation.service.ReservationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ReservationControllerTest {

    @Mock
    private ReservationService reservationService;
    @InjectMocks
    private ReservationController reservationController;

    @Test
    void addReservation() {
        when(reservationService.addReservation(any())).thenAnswer(arguments -> arguments.getArgument(0));

        ResponseEntity<ReservationDto> reservationResponse = reservationController.addReservation(ReservationDto.builder()
                                                                                                                .id(1L)
                                                                                                                .build());

        assertThat(reservationResponse.getStatusCodeValue()).isEqualTo(201);
        assertThat(reservationResponse.getHeaders()
                                      .getLocation()).isEqualTo(URI.create("/reservations/1"));
        assertThat(reservationResponse.getBody()).isEqualTo(ReservationDto.builder()
                                                                          .id(1L)
                                                                          .build());
    }
}