package com.daniel.reservation.service;

import com.daniel.dinnerTable.service.DinnerTableService;
import com.daniel.dto.DinnerTableDto;
import com.daniel.dto.ReservationDto;
import com.daniel.dto.RestaurantDto;
import com.daniel.exceptions.RestaurantNotFoundException;
import com.daniel.mappers.ReservationMapper;
import com.daniel.model.DinnerTable;
import com.daniel.model.OpeningTime;
import com.daniel.model.Reservation;
import com.daniel.model.Restaurant;
import com.daniel.openingTime.service.OpeningTimeService;
import com.daniel.reservation.repository.ReservationRepository;
import com.daniel.restaurant.repository.RestaurantRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReservationServiceTest {

    @Mock
    private RestaurantRepository restaurantRepository;
    @Mock
    private DinnerTableService dinnerTableService;
    @Mock
    private OpeningTimeService openingTimeService;
    @Mock
    private ReservationRepository reservationRepository;
    @Mock
    private ReservationMapper reservationMapper;
    @InjectMocks
    private ReservationService reservationService;

    @Test
    void addReservation() {
        when(restaurantRepository.findById(anyLong())).thenReturn(Optional.of(Restaurant.builder()
                                                                                        .build()));
        when(openingTimeService.checkIfRestaurantIsOpenOnDate(any())).thenReturn(OpeningTime.builder()
                                                                                            .build());
        when(dinnerTableService.returnTableIfNotReserved(any(), any(), any())).thenReturn(DinnerTable.builder()
                                                                                                     .build());
        when(reservationMapper.convertDtoToEntity(any())).thenReturn(Reservation.builder()
                                                                                .build());
        when(reservationRepository.save(any())).thenAnswer(arguments -> arguments.getArgument(0));
        when(reservationMapper.convertEntityToDto(any())).thenReturn(ReservationDto.builder()
                                                                                   .build());

        ReservationDto reservationDto = ReservationDto.builder()
                                                      .dinnerTable(DinnerTableDto.builder()
                                                                                 .id(2L)
                                                                                 .build())
                                                      .restaurant(RestaurantDto.builder()
                                                                               .id(1L)
                                                                               .build())
                                                      .build();
        ReservationDto actualReservationDto = reservationService.addReservation(reservationDto);

        assertThat(actualReservationDto).isNotNull();
        verify(restaurantRepository, times(1)).findById(1L);
        verify(reservationMapper, times(1)).convertDtoToEntity(reservationDto);
        verify(reservationMapper, times(1)).convertEntityToDto(any());
    }

    @Test
    void addReservation_returnsRestaurantNotFoundExceptionWhenRestaurantNotFound() {
        when(restaurantRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));
        when(openingTimeService.checkIfRestaurantIsOpenOnDate(any())).thenReturn(OpeningTime.builder()
                                                                                            .build());
        when(dinnerTableService.returnTableIfNotReserved(any(), any(), any())).thenReturn(DinnerTable.builder()
                                                                                                     .build());
        when(reservationMapper.convertDtoToEntity(any())).thenReturn(Reservation.builder()
                                                                                .build());

        assertThrows(RestaurantNotFoundException.class, () -> reservationService.addReservation(ReservationDto.builder()
                                                                                                              .dinnerTable(DinnerTableDto.builder()
                                                                                                                                         .id(2L)
                                                                                                                                         .build())
                                                                                                              .restaurant(RestaurantDto.builder()
                                                                                                                                       .id(1L)
                                                                                                                                       .build())
                                                                                                              .build()));
    }
}