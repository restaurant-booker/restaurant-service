package com.daniel.restaurant.controller;

import com.daniel.dto.RestaurantDto;
import com.daniel.restaurant.service.RestaurantService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RestaurantControllerTest {

    @Mock
    private RestaurantService restaurantService;

    @InjectMocks
    private RestaurantController restaurantController;

    @Test
    void getAllRestaurantsBy_returnsRestaurants() {
        when(restaurantService.getAllRestaurantsBy(null,
                                                   null,
                                                   null,
                                                   null)).thenReturn(Arrays.asList(RestaurantDto.builder()
                                                                                                .name("0")
                                                                                                .build(),
                                                                                   RestaurantDto.builder()
                                                                                                .name("1")
                                                                                                .build()));

        List<RestaurantDto> restaurants = restaurantController.getAllRestaurantsBy(null,
                                                                                   null,
                                                                                   null,
                                                                                   null);

        assertThat(restaurants).isNotEmpty();
        assertThat(restaurants).hasSize(2);
        assertThat(restaurants).containsExactlyInAnyOrder(RestaurantDto.builder()
                                                                       .name("0")
                                                                       .build(),
                                                          RestaurantDto.builder()
                                                                       .name("1")
                                                                       .build());
    }

    @Test
    void getAllRestaurantsBy_returnsEmptyFluxWhenNoRestaurants() {
        when(restaurantService.getAllRestaurantsBy(null,
                                                   null,
                                                   null,
                                                   null)).thenReturn(Collections.emptyList());

        List<RestaurantDto> restaurants = restaurantController.getAllRestaurantsBy(null,
                                                                                   null,
                                                                                   null,
                                                                                   null);

        assertThat(restaurants).isEmpty();
    }

    @Test
    void addRestaurant() {
        when(restaurantService.addRestaurant(any())).thenAnswer(arguments -> arguments.getArgument(0));

        ResponseEntity<RestaurantDto> restaurantResponse = restaurantController.addRestaurant(RestaurantDto.builder()
                                                                                                           .id(1L)
                                                                                                           .build());

        assertThat(restaurantResponse.getStatusCodeValue()).isEqualTo(201);
        assertThat(restaurantResponse.getHeaders()
                                     .getLocation()).isEqualTo(URI.create("/restaurants/1"));
        assertThat(restaurantResponse.getBody()).isEqualTo(RestaurantDto.builder()
                                                                        .id(1L)
                                                                        .build());
    }

    @Test
    void getRestaurant_returnsOkWhenRestaurantExists() {
        when(restaurantService.getRestaurant(any())).thenReturn(Optional.of(RestaurantDto.builder()
                                                                                         .id(1L)
                                                                                         .build()));

        ResponseEntity<RestaurantDto> restaurantResponse = restaurantController.getRestaurantById(1L);

        assertThat(restaurantResponse.getStatusCodeValue()).isEqualTo(200);
        assertThat(restaurantResponse.getBody()
                                     .getId()).isEqualTo(1L);
    }

    @Test
    void getRestaurant_returnsNotFoundWhenRestaurantDoesNotExist() {
        when(restaurantService.getRestaurant(any())).thenReturn(Optional.empty());

        ResponseEntity<RestaurantDto> restaurantResponse = restaurantController.getRestaurantById(1L);

        assertThat(restaurantResponse.getStatusCodeValue()).isEqualTo(404);
        assertThat(restaurantResponse.getBody()).isNull();
    }

}
