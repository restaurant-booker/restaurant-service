package com.daniel.restaurant.service;

import com.daniel.dto.RestaurantDto;
import com.daniel.mappers.RestaurantMapper;
import com.daniel.model.Category;
import com.daniel.model.Restaurant;
import com.daniel.restaurant.repository.RestaurantRepository;
import com.daniel.restaurant.repository.RestaurantSpecs;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RestaurantServiceTest {

    @Mock
    private RestaurantMapper restaurantMapper;

    @Mock
    private RestaurantSpecs restaurantSpecs;

    @Mock
    private RestaurantRepository restaurantRepository;

    @InjectMocks
    private RestaurantService restaurantService;

    @Test
    void addRestaurant() {

        when(restaurantMapper.convertEntityToDto(any())).thenReturn(RestaurantDto.builder()
                                                                                 .build());
        when(restaurantMapper.convertDtoToEntity(any())).thenReturn(Restaurant.builder()
                                                                              .build());
        when(restaurantRepository.save(any())).thenAnswer(arguments -> arguments.getArgument(0));

        RestaurantDto restaurantDto = RestaurantDto.builder()
                                                   .build();

        RestaurantDto actualRestaurant = restaurantService.addRestaurant(restaurantDto);

        assertThat(actualRestaurant).isNotNull();
        verify(restaurantRepository, times(1)).save(any());
        verify(restaurantMapper, times(1)).convertEntityToDto(any());
        verify(restaurantMapper, times(1)).convertDtoToEntity(any());
    }

    @Test
    void getRestaurant() {

        when(restaurantRepository.findById(any())).thenReturn(Optional.of(Restaurant.builder()
                                                                                    .build()));

        when(restaurantMapper.convertEntityToDto(any())).thenReturn(RestaurantDto.builder()
                                                                                 .build());

        Optional<RestaurantDto> restaurantDto = restaurantService.getRestaurant(1L);

        assertThat(restaurantDto.isPresent()).isTrue();
        verify(restaurantMapper, times(1)).convertEntityToDto(any());
    }

    @Test
    void getAvailableRestaurantsBy() {

        Date date = Date.valueOf(LocalDate.now());
        Time startTime = Time.valueOf(LocalTime.now());
        String name = "name";
        Category category = Category.AMERICAN;
        Restaurant restaurant = Restaurant.builder()
                                          .build();

        when(restaurantSpecs.availableRestaurants(any(), any())).thenReturn(null);
        when(restaurantSpecs.nameIsLike(any())).thenReturn(null);
        when(restaurantSpecs.categoryEquals(any())).thenReturn(null);
        when(restaurantMapper.convertEntityToDto(any())).thenReturn(RestaurantDto.builder().build());
        when(restaurantRepository.findAll(any(Specification.class)))
                                 .thenReturn(Collections.singletonList(restaurant));

        List<RestaurantDto> restaurants = restaurantService.getAllRestaurantsBy(name, category, date, startTime);

        assertThat(restaurants).hasSize(1);
        verify(restaurantSpecs, times(1)).availableRestaurants(date, startTime);
        verify(restaurantSpecs, times(1)).nameIsLike(name);
        verify(restaurantSpecs, times(1)).categoryEquals(category);
        verify(restaurantMapper, times(1)).convertEntityToDto(restaurant);
        verify(restaurantRepository, times(1)).findAll(any(Specification.class));
    }

    @Test
    void getAllRestaurantsBy_returnsEmptyFluxWhenNoRestaurants() {

        Date date = Date.valueOf(LocalDate.now());
        Time startTime = Time.valueOf(LocalTime.now());
        String name = "name";
        Category category = Category.AMERICAN;

        when(restaurantSpecs.availableRestaurants(any(), any())).thenReturn(null);
        when(restaurantSpecs.nameIsLike(any())).thenReturn(null);
        when(restaurantSpecs.categoryEquals(any())).thenReturn(null);

        when(restaurantRepository.findAll(any(Specification.class))).thenReturn(Collections.emptyList());

        List<RestaurantDto> restaurants = restaurantService.getAllRestaurantsBy(name, category, date, startTime);

        assertThat(restaurants).isEmpty();
    }
}
