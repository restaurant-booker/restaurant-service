package com.daniel.restaurant.repository;

import com.daniel.model.Category;
import com.daniel.model.OpeningTime;
import com.daniel.model.Reservation;
import com.daniel.model.Restaurant;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Date;
import java.sql.Time;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest(showSql = false, properties = {
        "logging.level.org.springframework=OFF",
        "logging.level.root=OFF"
})
class RestaurantSpecsTest {

    @Autowired
    private RestaurantRepository restaurantRepository;

    private final RestaurantSpecs restaurantSpecs = new RestaurantSpecs();

    @Test
    void availableRestaurants_doesntFilterRestaurantsIfReservationDateIsNull() {

        Restaurant restaurant1 = createRestaurant(0, 0);
        Restaurant restaurant2 = createRestaurant(1, 1);
        Restaurant restaurant3 = createRestaurant(0, 1);

        restaurantRepository.save(restaurant1);
        restaurantRepository.save(restaurant2);
        restaurantRepository.save(restaurant3);

        List<Restaurant> restaurants = restaurantRepository.findAll(restaurantSpecs.availableRestaurants(null,
                                                                                                         Time.valueOf(LocalTime.of(14, 0, 0)
                                                                                                                               .plusHours(1))));

        assertThat(restaurants).hasSize(3);
    }

    @Test
    void availableRestaurants_doesntFilterRestaurantsIfReservationStartTimeIsNull() {

        Restaurant restaurant1 = createRestaurant(0, 0);
        Restaurant restaurant2 = createRestaurant(1, 1);
        Restaurant restaurant3 = createRestaurant(0, 1);

        restaurantRepository.save(restaurant1);
        restaurantRepository.save(restaurant2);
        restaurantRepository.save(restaurant3);

        List<Restaurant> restaurants = restaurantRepository.findAll(restaurantSpecs.availableRestaurants(Date.valueOf(LocalDate.of(2019, 11, 11)
                                                                                                                               .plusDays(0)),
                                                                                                         null));

        assertThat(restaurants).hasSize(3);
    }

    @Test
    void availableRestaurants_returnsReservableRestaurantsOnly() {

        Restaurant restaurant1 = createRestaurant(0, 0);
        Restaurant restaurant2 = createRestaurant(1, 1);
        Restaurant restaurant3 = createRestaurant(0, 1);

        restaurantRepository.save(restaurant1);
        restaurantRepository.save(restaurant2);
        restaurantRepository.save(restaurant3);

        List<Restaurant> restaurants = restaurantRepository.findAll(restaurantSpecs.availableRestaurants(Date.valueOf(LocalDate.of(2019, 11, 11)
                                                                                                                               .plusDays(0)),
                                                                                                         Time.valueOf(LocalTime.of(14, 0, 0))));

        assertThat(restaurants).hasSize(1);
        assertThat(restaurants.get(0)).isEqualTo(restaurant2);
    }

    @Test
    void availableRestaurants_returnRestaurantIfLastReservationWasMadeForTwoHoursAgo() {

        Restaurant restaurant = createRestaurant(0, 0);

        restaurantRepository.save(restaurant);

        Optional<Restaurant> actualRestaurant = restaurantRepository.findOne(restaurantSpecs.availableRestaurants(Date.valueOf(LocalDate.of(2019, 11, 11)
                                                                                                                                        .plusDays(0)),
                                                                                                                  Time.valueOf(LocalTime.of(14, 0, 0)
                                                                                                                                        .plusHours(2))));

        assertThat(actualRestaurant.isPresent()).isTrue();
    }

    @Test
    void availableRestaurants_doesntReturnRestaurantIfReservationIsInProgress() {

        Restaurant restaurant = createRestaurant(0, 0);

        restaurantRepository.save(restaurant);

        Optional<Restaurant> actualRestaurant = restaurantRepository.findOne(restaurantSpecs.availableRestaurants(Date.valueOf(LocalDate.of(2019, 11, 11)
                                                                                                                                        .plusDays(0)),
                                                                                                                  Time.valueOf(LocalTime.of(14, 0, 0)
                                                                                                                                        .minusHours(1))));

        assertThat(actualRestaurant.isPresent()).isFalse();
    }

    @Test
    void availableRestaurants_returnsRestaurantWhenReservationStartTimeNotColliding() {

        Date date = Date.valueOf(LocalDate.of(2019, 11, 11));

        Restaurant restaurant = createRestaurant(0, 0);
        restaurant.setReservations(Arrays.asList(Reservation.builder()
                                                            .date(date)
                                                            .startTime(Time.valueOf(LocalTime.of(14, 0, 0)))
                                                            .restaurant(restaurant)
                                                            .build(),
                                                 Reservation.builder()
                                                            .date(date)
                                                            .startTime(Time.valueOf(LocalTime.of(14, 0, 0)
                                                                                             .plusHours(1)))
                                                            .restaurant(restaurant)
                                                            .build(),
                                                 Reservation.builder()
                                                            .date(date)
                                                            .startTime(Time.valueOf(LocalTime.of(14, 0, 0)
                                                                                             .plusHours(2)))
                                                            .restaurant(restaurant)
                                                            .build()));


        restaurantRepository.save(restaurant);

        restaurantRepository.findAll();

        List<Restaurant> restaurants = restaurantRepository.findAll(restaurantSpecs.availableRestaurants(Date.valueOf(LocalDate.of(2019, 11, 11)
                                                                                                                               .plusDays(0)),
                                                                                                         Time.valueOf(LocalTime.of(18, 0, 0))));

        assertThat(restaurants).hasSize(1);
        assertThat(restaurants.get(0)).isEqualTo(restaurant);
    }

    @Test
    void availableRestaurants_returnsRestaurantIfThereAreNoReservations() {

        Restaurant restaurant = createRestaurant(0, 0);
        restaurant.setReservations(null);

        restaurantRepository.save(restaurant);

        Optional<Restaurant> actualRestaurant = restaurantRepository.findOne(restaurantSpecs.availableRestaurants(Date.valueOf(LocalDate.of(2019, 11, 11)
                                                                                                                                        .plusDays(0)),
                                                                                                                  Time.valueOf(LocalTime.of(14, 0, 0)
                                                                                                                                        .plusHours(1))));

        assertThat(actualRestaurant.isPresent()).isTrue();
    }

    @Test
    void availableRestaurants_doesntReturnRestaurantIfReservationDoesntMatchOpeningTimes() {

        Restaurant restaurant = createRestaurant(0, 0);
        restaurant.setReservations(null);

        restaurantRepository.save(restaurant);

        Optional<Restaurant> actualRestaurant = restaurantRepository.findOne(restaurantSpecs.availableRestaurants(Date.valueOf(LocalDate.of(2019, 11, 11)
                                                                                                                                        .plusDays(0)),
                                                                                                                  Time.valueOf(LocalTime.of(19, 0, 0))));

        assertThat(actualRestaurant.isPresent()).isFalse();
    }

    @Test
    void availableRestaurants_returnsRestaurantWhenReservationStartTimeFitsOpeningTimesAndThereAreNoReservationsOnDay() {

        Restaurant restaurant = createRestaurant(0, 0);
        restaurant.setReservations(null);

        restaurantRepository.save(restaurant);

        Optional<Restaurant> actualRestaurant = restaurantRepository.findOne(restaurantSpecs.availableRestaurants(Date.valueOf(LocalDate.of(2019, 11, 11)
                                                                                                                                        .plusDays(0)),
                                                                                                                  Time.valueOf(LocalTime.of(18, 0, 0))));

        assertThat(actualRestaurant.isPresent()).isTrue();
    }

    @Test
    void nameIsLike_returnsRestaurantIfContainsTheNameString() {

        Restaurant restaurant = createRestaurant(0, 0);
        restaurant.setName("My restaurant name");

        restaurantRepository.save(restaurant);

        Optional<Restaurant> actualRestaurant = restaurantRepository.findOne(restaurantSpecs.nameIsLike("name"));

        assertThat(actualRestaurant.isPresent()).isTrue();
    }

    @Test
    void nameIsLike_doesntFindARestaurantByNameIfNameIsNull() {

        Restaurant restaurant = createRestaurant(0, 0);
        restaurant.setName("different");

        restaurantRepository.save(restaurant);

        Optional<Restaurant> actualRestaurant = restaurantRepository.findOne(restaurantSpecs.nameIsLike(null));

        assertThat(actualRestaurant.get().getName()).isEqualTo("different");
    }

    @Test
    void categoryEquals_doesntFindARestaurantByCategoryIfCategoryIsNull() {

        Restaurant restaurant = createRestaurant(0, 0);
        restaurant.setCategories(Set.of(Category.JAPANESE));

        restaurantRepository.save(restaurant);

        Optional<Restaurant> actualRestaurant = restaurantRepository.findOne(restaurantSpecs.categoryEquals(null));

        assertThat(actualRestaurant.isPresent()).isTrue();
    }

    @Test
    void categoryEquals_returnsRestaurantIfContainsTheCategory() {

        Restaurant restaurant = createRestaurant(0, 0);
        restaurant.setCategories(Set.of(Category.AMERICAN, Category.ITALIAN));

        restaurantRepository.save(restaurant);

        Optional<Restaurant> actualRestaurant = restaurantRepository.findOne(restaurantSpecs.categoryEquals(Category.AMERICAN));

        assertThat(actualRestaurant.isPresent()).isTrue();
    }


    private Restaurant createRestaurant(Integer daysToAdd, Integer hoursToAdd) {
        Date date = Date.valueOf(LocalDate.of(2019, 11, 11)
                                          .plusDays(daysToAdd));
        Time startTime = Time.valueOf(LocalTime.of(14, 0, 0)
                                               .plusHours(hoursToAdd));

        Restaurant restaurant = Restaurant.builder()
                                          .build();

        Reservation reservation = Reservation.builder()
                                             .date(date)
                                             .startTime(startTime)
                                             .restaurant(restaurant)
                                             .build();
        OpeningTime openingTime = OpeningTime.builder()
                                             .dayOfWeek(DayOfWeek.MONDAY)
                                             .startTime(Time.valueOf(LocalTime.of(8, 0, 0)))
                                             .closingTime(Time.valueOf(LocalTime.of(20, 0, 0)))
                                             .restaurant(restaurant)
                                             .build();

        restaurant.setOpeningTimes(Collections.singleton(openingTime));
        restaurant.setReservations(Collections.singletonList(reservation));
        return restaurant;
    }

}