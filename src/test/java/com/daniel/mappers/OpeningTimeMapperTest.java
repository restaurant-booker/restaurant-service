package com.daniel.mappers;

import com.daniel.dto.OpeningTimeDto;
import com.daniel.model.OpeningTime;
import org.junit.jupiter.api.Test;

import java.sql.Time;
import java.time.DayOfWeek;
import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

class OpeningTimeMapperTest {

    private static final OpeningTimeMapper openingTimeMapper = new OpeningTimeMapper();

    @Test
    void convertDtoToEntity() {

        OpeningTimeDto openingTimeDto = OpeningTimeDto.builder()
                                                      .id(1L)
                                                      .startTime(Time.valueOf(LocalTime.now()))
                                                      .dayOfWeek(DayOfWeek.MONDAY)
                                                      .closingTime(Time.valueOf(LocalTime.now()))
                                                      .build();

        OpeningTime openingTime = openingTimeMapper.convertDtoToEntity(openingTimeDto);

        assertThat(openingTime).isEqualToComparingFieldByField(openingTime);
    }

    @Test
    void convertEntityToDto() {

        OpeningTime openingTime = OpeningTime.builder()
                                             .id(1L)
                                             .startTime(Time.valueOf(LocalTime.now()))
                                             .dayOfWeek(DayOfWeek.MONDAY)
                                             .closingTime(Time.valueOf(LocalTime.now()))
                                             .build();

        OpeningTimeDto openingTimeDto = openingTimeMapper.convertEntityToDto(openingTime);

        assertThat(openingTimeDto).isEqualToComparingFieldByField(openingTime);
    }
}