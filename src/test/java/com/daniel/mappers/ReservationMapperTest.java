package com.daniel.mappers;

import com.daniel.dto.DinnerTableDto;
import com.daniel.dto.ReservationDto;
import com.daniel.dto.RestaurantDto;
import com.daniel.model.DinnerTable;
import com.daniel.model.Reservation;
import com.daniel.model.Restaurant;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

class ReservationMapperTest {

    private final ReservationMapper reservationMapper = new ReservationMapper();

    @Test
    void convertEntityToDto_doesntConvertNullValues() {

        Reservation reservation = Reservation.builder()
                                             .id(1L)
                                             .date(Date.valueOf(LocalDate.now()))
                                             .startTime(Time.valueOf(LocalTime.now()))
                                             .build();

        ReservationDto reservationDto = reservationMapper.convertEntityToDto(reservation);

        assertThat(reservationDto).isEqualToIgnoringGivenFields(reservation, "restaurantDto", "dinnerTableDto");
        assertThat(reservationDto.getRestaurant()).isNull();
        assertThat(reservationDto.getDinnerTable()).isNull();
    }

    @Test
    void convertEntityToDto() {

        long id = 1L;
        String restaurantName = "name";
        Reservation reservation = Reservation.builder()
                                             .id(id)
                                             .date(Date.valueOf(LocalDate.now()))
                                             .startTime(Time.valueOf(LocalTime.now()))
                                             .restaurant(Restaurant.builder()
                                                                   .id(id)
                                                                   .name(restaurantName)
                                                                   .build())
                                             .dinnerTable(DinnerTable.builder()
                                                                     .id(2L)
                                                                     .seats(2)
                                                                     .build())
                                             .build();

        ReservationDto reservationDto = reservationMapper.convertEntityToDto(reservation);

        assertThat(reservationDto).isEqualToIgnoringGivenFields(reservation, "restaurant", "dinnerTable");
        RestaurantDto restaurantDto = reservationDto.getRestaurant();
        assertThat(restaurantDto.getId()).isEqualTo(id);
        assertThat(restaurantDto.getName()).isEqualTo(restaurantName);
        DinnerTableDto dinnerTableDto = reservationDto.getDinnerTable();
        assertThat(dinnerTableDto.getId()).isEqualTo(2L);
        assertThat(dinnerTableDto.getSeats()).isEqualTo(2);
    }

    @Test
    void convertDtoToEntity() {

        ReservationDto reservationDto = ReservationDto.builder()
                                                      .id(1L)
                                                      .date(Date.valueOf(LocalDate.now()))
                                                      .startTime(Time.valueOf(LocalTime.now()))
                                                      .build();

        Reservation reservation = reservationMapper.convertDtoToEntity(reservationDto);

        assertThat(reservation).isEqualToIgnoringGivenFields(reservationDto, "dinnerTable", "restaurant");

    }
}