package com.daniel.mappers;

import com.daniel.dto.DinnerTableDto;
import com.daniel.model.DinnerTable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class DinnerTableMapperTest {

    @InjectMocks
    private DinnerTableMapper dinnerTableMapper;

    @Test
    void convertDtoToEntity() {

        DinnerTableDto dinnerTableDto = DinnerTableDto.builder()
                                                      .id(1L)
                                                      .seats(2)
                                                      .build();

        DinnerTable dinnerTable = dinnerTableMapper.convertDtoToEntity(dinnerTableDto);

        assertThat(dinnerTable).isEqualToIgnoringGivenFields(dinnerTableDto, "reservations", "restaurant");
    }

    @Test
    void convertEntityToDto() {

        DinnerTable dinnerTable = DinnerTable.builder()
                                             .id(1L)
                                             .seats(2)
                                             .build();

        DinnerTableDto dinnerTableDto = dinnerTableMapper.convertEntityToDto(dinnerTable);

        assertThat(dinnerTableDto).isEqualToIgnoringGivenFields(dinnerTable, "reservations", "restaurant");
    }
}