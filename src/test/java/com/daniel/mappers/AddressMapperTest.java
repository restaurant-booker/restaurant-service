package com.daniel.mappers;

import com.daniel.dto.AddressDto;
import com.daniel.model.Address;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AddressMapperTest {

    private final AddressMapper addressMapper = new AddressMapper();

    @Test
    void convertDtoToEntity() {

        Address address = addressMapper.convertDtoToEntity(AddressDto.builder()
                                                                      .addressLine1("Line1")
                                                                      .addressLine2("Line2")
                                                                      .addressLine3("Line3")
                                                                      .addressLine4("Line4")
                                                                      .postcode("postcode")
                                                                      .id(1L)
                                                                      .country("Poland")
                                                                      .city("Poznan")
                                                                      .build());

        assertThat(address.getAddressLine1()).isEqualTo("Line1");
        assertThat(address.getAddressLine2()).isEqualTo("Line2");
        assertThat(address.getAddressLine3()).isEqualTo("Line3");
        assertThat(address.getAddressLine4()).isEqualTo("Line4");
        assertThat(address.getPostcode()).isEqualTo("postcode");
        assertThat(address.getId()).isEqualTo(1L);
        assertThat(address.getCountry()).isEqualTo("Poland");
        assertThat(address.getCity()).isEqualTo("Poznan");
    }

    @Test
    void convertEntityToDto() {
        AddressDto addressDto = addressMapper.convertEntityToDto(Address.builder()
                                                                     .addressLine1("Line1")
                                                                     .addressLine2("Line2")
                                                                     .addressLine3("Line3")
                                                                     .addressLine4("Line4")
                                                                     .postcode("postcode")
                                                                     .id(1L)
                                                                     .country("Poland")
                                                                     .city("Poznan")
                                                                     .build());

        assertThat(addressDto.getAddressLine1()).isEqualTo("Line1");
        assertThat(addressDto.getAddressLine2()).isEqualTo("Line2");
        assertThat(addressDto.getAddressLine3()).isEqualTo("Line3");
        assertThat(addressDto.getAddressLine4()).isEqualTo("Line4");
        assertThat(addressDto.getPostcode()).isEqualTo("postcode");
        assertThat(addressDto.getId()).isEqualTo(1L);
        assertThat(addressDto.getCountry()).isEqualTo("Poland");
        assertThat(addressDto.getCity()).isEqualTo("Poznan");
    }
}
