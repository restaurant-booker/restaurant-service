package com.daniel.mappers;

import com.daniel.dto.AddressDto;
import com.daniel.dto.DinnerTableDto;
import com.daniel.dto.OpeningTimeDto;
import com.daniel.dto.ReservationDto;
import com.daniel.dto.RestaurantDto;
import com.daniel.model.Address;
import com.daniel.model.Category;
import com.daniel.model.DinnerTable;
import com.daniel.model.OpeningTime;
import com.daniel.model.Reservation;
import com.daniel.model.Restaurant;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RestaurantMapperTest {

    @Mock
    private AddressMapper addressMapper;
    @Mock
    private DinnerTableMapper dinnerTableMapper;
    @Mock
    private OpeningTimeMapper openingTimeMapper;
    @Mock
    private ReservationMapper reservationMapper;
    @InjectMocks
    private RestaurantMapper restaurantMapper;

    @Test
    void convertRestaurantToRestaurantDto_convertsAlsoOtherEntities() {

        when(addressMapper.convertEntityToDto(any())).thenReturn(AddressDto.builder().build());
        when(dinnerTableMapper.convertEntityToDto(any())).thenReturn(DinnerTableDto.builder().build());
        when(openingTimeMapper.convertEntityToDto(any())).thenReturn(OpeningTimeDto.builder().build());
        when(reservationMapper.convertEntityToDto(any())).thenReturn(ReservationDto.builder().build());

        Restaurant restaurant = Restaurant.builder()
                                          .name("name")
                                          .categories(Collections.singleton(Category.AMERICAN))
                                          .id(1L)
                                          .address(Address.builder()
                                                          .build())
                                          .dinnerTables(Collections.singletonList(DinnerTable.builder()
                                                                                             .build()))
                                          .openingTimes(Collections.singleton(OpeningTime.builder()
                                                                                         .build()))
                                          .reservations(Collections.singletonList(Reservation.builder()
                                                                                             .build()))
                                          .build();

        RestaurantDto restaurantDto = restaurantMapper.convertEntityToDto(restaurant);

        assertThat(restaurantDto).isEqualToIgnoringGivenFields(restaurant, "address", "dinnerTables", "openingTimes", "reservations");
        verify(addressMapper, times(1)).convertEntityToDto(any());
        verify(dinnerTableMapper, times(1)).convertEntityToDto(any());
        verify(openingTimeMapper, times(1)).convertEntityToDto(any());
        verify(reservationMapper, times(1)).convertEntityToDto(any());

    }

    @Test
    void convertRestaurantDtoToRestaurant_convertsAlsoAddressDto() {

        when(addressMapper.convertDtoToEntity(any())).thenReturn(Address.builder().build());
        when(dinnerTableMapper.convertDtoToEntity(any())).thenReturn(DinnerTable.builder().build());
        when(openingTimeMapper.convertDtoToEntity(any())).thenReturn(OpeningTime.builder().build());

        RestaurantDto restaurantDto = RestaurantDto.builder()
                                          .name("name")
                                          .categories(Collections.singleton(Category.AMERICAN))
                                          .id(1L)
                                          .address(AddressDto.builder()
                                                          .build())
                                          .dinnerTables(Collections.singletonList(DinnerTableDto.builder()
                                                                                             .build()))
                                          .openingTimes(Collections.singleton(OpeningTimeDto.builder()
                                                                                         .build()))
                                          .build();

        Restaurant restaurant = restaurantMapper.convertDtoToEntity(restaurantDto);

        assertThat(restaurant).isEqualToIgnoringGivenFields(restaurantDto, "address", "dinnerTables", "openingTimes");
        verify(addressMapper, times(1)).convertDtoToEntity(any());
        verify(dinnerTableMapper, times(1)).convertDtoToEntity(any());
        verify(openingTimeMapper, times(1)).convertDtoToEntity(any());
    }
}
