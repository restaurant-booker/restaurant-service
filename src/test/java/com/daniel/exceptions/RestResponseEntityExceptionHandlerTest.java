package com.daniel.exceptions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
class RestResponseEntityExceptionHandlerTest {

    private RestResponseEntityExceptionHandler restResponseEntityExceptionHandler;

    @BeforeEach
    void setup() {
        restResponseEntityExceptionHandler = new RestResponseEntityExceptionHandler();
    }

    @Test
    void handleNotFound_handlesRestaurantNotFoundException() {
        ResponseEntity responseEntity = restResponseEntityExceptionHandler.handleNotFound(new RestaurantNotFoundException(), null);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
        assertThat(responseEntity.getBody()).isEqualTo("The required restaurant has not been found.");
    }

    @Test
    void handleNotFound_handlesDinnerTableNotFoundException() {
        ResponseEntity responseEntity = restResponseEntityExceptionHandler.handleNotFound(new DinnerTableNotFoundException(), null);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
        assertThat(responseEntity.getBody()).isEqualTo("The required dinner table has not been found.");
    }

    @Test
    void handleForbidden_handlesRestaurantNotOpenException() {
        ResponseEntity responseEntity = restResponseEntityExceptionHandler.handleForbidden(new RestaurantNotOpenException(), null);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(403);
        assertThat(responseEntity.getBody()).isEqualTo("This restaurant is not open on this day.");
    }

    @Test
    void handleForbidden_handlesDinnerTableAlreadyReservedException() {
        ResponseEntity responseEntity = restResponseEntityExceptionHandler.handleForbidden(new DinnerTableAlreadyReservedException(), null);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(403);
        assertThat(responseEntity.getBody()).isEqualTo("This dinner table is already reserved at this time.");
    }
}